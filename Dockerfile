FROM amanu/php56-apache

COPY src/ /var/www/html/

RUN chown -R apache:apache /var/www/html && chmod 755 -R /var/www/html

VOLUME /var/www/html/
